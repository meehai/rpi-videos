stem="${1:0:-4}"
out_file="$stem".mkv
echo "In file: $1"
echo "Out file: $out_file"
ffmpeg -i "$1" -c:v libvpx-vp9 -pix_fmt yuv420p -crf 23 -pass 1 -an -f null /dev/null
ffmpeg -i "$1" -c:v libvpx-vp9 -pix_fmt yuv420p -crf 23 -pass 2 -c:a libopus "$stem".mkv
ffmpeg -i "$1" "$stem".srt

